package finishingtimecalculator;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateTimeService {

	// Convert Date object to Calendar object
	public Calendar dateToCalendar(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar;
	}

	// Convert Calendar object to Date object
	public Date calendarToDate(Calendar calendar) {
		return calendar.getTime();
	}

	public boolean isWorkingHours(Date date) {
		if (date.getHours() >= 9 && date.getHours() <= 16) {
			return true;
		}
		return false;
	}

	public boolean isWeekend(java.util.Date date) {
		Calendar cal = new java.util.GregorianCalendar();
		cal.setTime(date);
		if ((Calendar.SATURDAY == cal.get(Calendar.DAY_OF_WEEK))
				|| (Calendar.SUNDAY == cal.get(Calendar.DAY_OF_WEEK))) {
			return (true);
		} else {
			return false;
		}
	}

	public boolean isValidWorkingTime(Calendar calendar) {
		if (!isWeekend(calendarToDate(calendar)) && isWorkingHours(calendarToDate(calendar))) {
			return true;
		}
		return false;
	}

	public String finishingTimeCalculator(String submitDateTime, int turnAroundTimeInHours) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
		Date resultDateTime = sdf.parse(submitDateTime);

		// Convert Date object to Calendar object - in order to ADD HOUR
		Calendar calendar = dateToCalendar(resultDateTime);
		if (!isValidWorkingTime(calendar)) {
			throw new Exception(
					"\nInput Date/Time is NOT valid!\nWorking Hours needed between 9:00 and 16:59:59, and Date must exclude Weekends!\n");
		}

		int successAddedHourIterations = 1;
		while (successAddedHourIterations <= turnAroundTimeInHours) {
			if (isValidWorkingTime(calendar)) {
				calendar.add(Calendar.HOUR_OF_DAY, +1);
				successAddedHourIterations++;
			} else {
				calendar.add(Calendar.HOUR_OF_DAY, +1);
			}
		}

		// Convert back to String output
		resultDateTime = calendarToDate(calendar);
		return sdf.format(resultDateTime);
	}

}
