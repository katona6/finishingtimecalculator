package finishingtimecalculator;

import java.text.ParseException;

public class Main {

	public static void main(String[] args) {
		DateTimeService dts = new DateTimeService();
	      try {
	    	System.out.println("2021.04.08 15:00:00");
			System.out.println("+ 3 hour");
			System.out.println(dts.finishingTimeCalculator("2021.04.08 15:00:00",3));
			System.out.println();
			System.out.println("2021.04.09 16:00:00");
			System.out.println("+ 3 hour");
			System.out.println(dts.finishingTimeCalculator("2021.04.09 16:00:00",3));
			System.out.println();
			System.out.println("2021.04.05 09:00:00");
			System.out.println("+ 16 hour");
			System.out.println(dts.finishingTimeCalculator("2021.04.05 09:00:00",16));
			System.out.println();
			System.out.println("2021.04.05 09:00:00");
			System.out.println("+ 25 hour");
			System.out.println(dts.finishingTimeCalculator("2021.04.05 09:00:00",25));
			System.out.println("---------------------------NONZERO-MINUTES---");
			System.out.println("2021.04.05 16:30:00");
			System.out.println("+ 2 hour");
			System.out.println(dts.finishingTimeCalculator("2021.04.05 16:30:00",2));
			System.out.println();
			System.out.println("2021.04.09 16:01:00");
			System.out.println("+ 2 hour");
			System.out.println(dts.finishingTimeCalculator("2021.04.09 16:01:00",2));
			
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	
	


	
}
