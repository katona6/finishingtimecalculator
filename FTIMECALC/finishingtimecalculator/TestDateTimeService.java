package finishingtimecalculator;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestDateTimeService {

	@Test
	public void testCalculateDueDate_on2021_04_08_15_00_00__plus3hr_assertEquals2021_04_09_10_00_00() throws Exception {
		DateTimeService dts = new DateTimeService();
		String input = "2021.04.08 15:00:00";
		int duration = 3;
		assertEquals("2021.04.09 10:00:00", dts.finishingTimeCalculator(input,duration));
	}
	@Test
	public void testCalculateDueDate_on2021_04_09_16_00_00__plus3hr_assertEquals2021_04_12_11_00_00() throws Exception {
		DateTimeService dts = new DateTimeService();
		String input = "2021.04.09 16:00:00";
		int duration = 3;
		assertEquals("2021.04.12 11:00:00", dts.finishingTimeCalculator(input,duration));
	}
	@Test
	public void testCalculateDueDate_on2021_04_05_09_00_00__plus16hr_assertEquals2021_04_06_17_00_00() throws Exception {
		DateTimeService dts = new DateTimeService();
		String input = "2021.04.05 09:00:00";
		int duration = 16;
		assertEquals("2021.04.06 17:00:00", dts.finishingTimeCalculator(input,duration));
	}
	@Test
	public void testCalculateDueDate_on2021_04_05_09_00_00__plus25hr_assertEquals2021_04_08_10_00_00() throws Exception {
		DateTimeService dts = new DateTimeService();
		String input = "2021.04.05 09:00:00";
		int duration = 25;
		assertEquals("2021.04.08 10:00:00", dts.finishingTimeCalculator(input,duration));
	}
	@Test
	public void testCalculateDueDate_on2021_04_05_16_30_00__plus2hr_assertEquals2021_04_06_10_30_00() throws Exception {
		DateTimeService dts = new DateTimeService();
		String input = "2021.04.05 16:30:00";
		int duration = 2;
		assertEquals("2021.04.06 10:30:00", dts.finishingTimeCalculator(input,duration));
	}
	@Test
	public void testCalculateDueDate_on2021_04_09_16_01_00__plus2hr_assertEquals2021_04_12_10_01_00() throws Exception {
		DateTimeService dts = new DateTimeService();
		String input = "2021.04.09 16:01:00";
		int duration = 2;
		assertEquals("2021.04.12 10:01:00", dts.finishingTimeCalculator(input,duration));
	}
	@Test
	public void testCalculateDueDate_on2021_04_06_10h_to14h_with2hrSteps() throws Exception {
		DateTimeService dts = new DateTimeService();
		int duration = 2;
		for (int i = 1; i <= 5; i++) {
			String input = "2021.04.06 "+Integer.valueOf(9 +i)+":00:00";
			assertEquals("2021.04.06 "+Integer.valueOf(11 +i)+":00:00", dts.finishingTimeCalculator(input,duration));
			
		}
		
	}

}
