# FinishingTimeCalculator #
## The problem ##
Your task is to implement the FinishingTimeCalculator method:
* Input: START TIME when we start working on a product & WORK-DURATION approximately how much time does it take to finishing an item.
* Output: Returns the TIME when the item is ready.

## RULES ##
* Working hours are from 9AM to 5PM on every workday, Monday to Friday,
* Holidays should be ignored at this time,
* The Work-Duration time is defined in working hours (e.g. 2 days equals 16 hours),
If working on an item starts at 2:12PM on Tuesday and the Work-Duration time is
16 hours, then it will be done by 2:12PM on Thursday.
* A Start time (Working on an item) can only be reported during working hours. (e.g. All submit date
values are set between 9AM to 5PM.)

## Additional info ##
* Solve problem in Java,
* Do not implement the user interface (Console Output enough),
* Including automated tests to your solution.
